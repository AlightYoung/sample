package com.example.a24631.testapp.RecyclerView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.Toast;

import com.example.a24631.testapp.R;

public class puRecyclerViewActivity extends AppCompatActivity {

    private RecyclerView puRv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pu_recycler_view);
        puRv=findViewById(R.id.puRv);
        puRv.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL ));
        puRv.setAdapter(new puAdapter(this, new puAdapter.onItemclickListener() {
            @Override
            public void onclick(int pos) {
                Toast.makeText(puRecyclerViewActivity.this, "坐标"+pos,Toast.LENGTH_SHORT).show();
            }
        }));
    }
}
