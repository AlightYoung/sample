package com.example.a24631.testapp.ListView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.a24631.testapp.R;

public class ListViewActivity extends AppCompatActivity {
    private ListView lv_1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        lv_1=findViewById(R.id.lv_1);
        lv_1.setAdapter(new MyListAdapter(ListViewActivity.this));
        lv_1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListViewActivity.this, "点击pos:"+position,Toast.LENGTH_SHORT).show();
            }
        });
        lv_1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> pareidnt, View view, int position, long id) {
                Toast.makeText(ListViewActivity.this, "长按pos:"+position,Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }
}
