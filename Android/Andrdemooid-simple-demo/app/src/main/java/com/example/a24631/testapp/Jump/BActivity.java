package com.example.a24631.testapp.Jump;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.a24631.testapp.R;

public class BActivity extends AppCompatActivity {

    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        tvTitle = findViewById(R.id.title);
        tvTitle.setText(getIntent().getStringExtra("extra"));
        Intent intent =new Intent();
        intent.putExtra("BExtra","B");
        setResult(1,intent);
    }
}
