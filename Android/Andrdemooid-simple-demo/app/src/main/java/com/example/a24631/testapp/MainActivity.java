package com.example.a24631.testapp;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.a24631.testapp.DataStorage.DataStorageActivity;
import com.example.a24631.testapp.GridView.GridViewActivity;
import com.example.a24631.testapp.Jump.AActivity;
import com.example.a24631.testapp.ListView.ExpandableListViewActivity;
import com.example.a24631.testapp.ListView.ListViewActivity;
import com.example.a24631.testapp.RecyclerView.RecyclerViewActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button ListViewBtn, GridViewBtn, RecyclerViewBtn, WebViewBtn;
    private Button ToastBtn, DialogBtn, CustomDialogBtn, ProgressBtn;
    private Button PopUpWindowBtn, ExpandableListViewBtn, ImageSwitcherBtn;
    private Button JumpBtn;
    private Button SpinnerBtn;
    private Button ViewPagerBtn;
    private Button HandlerBtn;
    private Button DataStorageBtn;

    private void initialize() {
        ListViewBtn = findViewById(R.id.btn_ListView);
        GridViewBtn = findViewById(R.id.btn_GridView);
        RecyclerViewBtn = findViewById(R.id.btn_RecyclerView);
        WebViewBtn = findViewById(R.id.btn_WebView);
        ToastBtn = findViewById(R.id.btn_toast);
        DialogBtn = findViewById(R.id.btn_dialog);
        ProgressBtn = findViewById(R.id.btn_progress);
        CustomDialogBtn = findViewById(R.id.btn_customDialog);
        PopUpWindowBtn = findViewById(R.id.btn_pop);
        ExpandableListViewBtn = findViewById(R.id.btn_ExpandableListView);
        ImageSwitcherBtn = findViewById(R.id.btn_ImageSwitcher);
        JumpBtn = findViewById(R.id.btn_jump);
        SpinnerBtn = findViewById(R.id.btn_spinner);
        ViewPagerBtn = findViewById(R.id.btn_viewpager);
        HandlerBtn = findViewById(R.id.btn_handler);
        DataStorageBtn = findViewById(R.id.btn_data_storage);

    }

    private void onclick() {
        ListViewBtn.setOnClickListener(this);
        GridViewBtn.setOnClickListener(this);
        RecyclerViewBtn.setOnClickListener(this);
        WebViewBtn.setOnClickListener(this);
        ToastBtn.setOnClickListener(this);
        DialogBtn.setOnClickListener(this);
        ProgressBtn.setOnClickListener(this);
        CustomDialogBtn.setOnClickListener(this);
        PopUpWindowBtn.setOnClickListener(this);
        ExpandableListViewBtn.setOnClickListener(this);
        JumpBtn.setOnClickListener(this);
        ImageSwitcherBtn.setOnClickListener(this);
        SpinnerBtn.setOnClickListener(this);
        ViewPagerBtn.setOnClickListener(this);
        HandlerBtn.setOnClickListener(this);
        DataStorageBtn.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
        initialize();
        onclick();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btn_ListView:
                intent = new Intent(this, ListViewActivity.class);
                break;
            case R.id.btn_GridView:
                intent = new Intent(this, GridViewActivity.class);
                break;
            case R.id.btn_RecyclerView:
                intent = new Intent(this, RecyclerViewActivity.class);
                break;
            case R.id.btn_WebView:
                intent = new Intent(this, WebViewActivity.class);
                break;
            case R.id.btn_toast:
                intent = new Intent(this, ToastActivity.class);
                break;
            case R.id.btn_dialog:
                intent = new Intent(this, DialogActivity.class);
                break;
            case R.id.btn_progress:
                intent = new Intent(this, ProgressActivity.class);
                break;
            case R.id.btn_customDialog:
                intent = new Intent(this, CustomDialogActivity.class);
                break;
            case R.id.btn_pop:
                intent = new Intent(this, PopUpWindowActivity.class);
                break;
            case R.id.btn_ExpandableListView:
                intent = new Intent(this, ExpandableListViewActivity.class);
                break;
            case R.id.btn_jump:
                intent = new Intent(this, AActivity.class);
                break;
            case R.id.btn_ImageSwitcher:
                intent = new Intent(this, ImageSwitcherActivity.class);
                break;
            case R.id.btn_spinner:
                intent = new Intent(this, SpinnerActivity.class);
                break;
            case R.id.btn_viewpager:
                intent = new Intent(this, ViewPagerActivity.class);
                break;
            case R.id.btn_handler:
                intent = new Intent(this, HandlerActivity.class);
                break;
            case R.id.btn_data_storage:
                intent = new Intent(this, DataStorageActivity.class);
                break;
        }
        startActivity(intent);
    }
}
