package com.example.a24631.testapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

import com.example.a24631.testapp.util.ToastUtil;

public class HandlerActivity extends AppCompatActivity {

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler);
        //1、延时处理

//        handler=new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startActivity(new Intent(HandlerActivity.this, MainActivity.class));
//            }
//        },3000);

        //2、线程间通信

        handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what){
                    case 1:
                        ToastUtil.showMsg(HandlerActivity.this, "线程通信成功");
                        break;
                }
            }
        };
        new Thread(){
            @Override
            public void run() {
                super.run();
                Message message = new Message();
                message.what=1;
                handler.sendMessage(message);
            }
        }.start();
        //

    }
}
