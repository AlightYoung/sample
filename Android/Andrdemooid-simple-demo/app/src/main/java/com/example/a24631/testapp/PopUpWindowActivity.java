package com.example.a24631.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.a24631.testapp.util.ToastUtil;

public class PopUpWindowActivity extends AppCompatActivity {

    private Button textPop1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_window);
        textPop1=findViewById(R.id.textPop1);
        textPop1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getLayoutInflater().inflate(R.layout.layout_pop,null);
                TextView textView = view.findViewById(R.id.tv_pop);
                final PopupWindow popupWindow = new PopupWindow(view);
                popupWindow.setWidth(textPop1.getWidth());
                popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        textPop1.setText("好");
                        ToastUtil.showMsg(PopUpWindowActivity.this, "你真明智");
                    }
                });
                popupWindow.setOutsideTouchable(true);
                popupWindow.setFocusable(false);
                //popupWindow.showAsDropDown(textPop1);
                popupWindow.showAtLocation(textPop1, textPop1.getGravity(),0,-100);
            }
        });
    }
}
