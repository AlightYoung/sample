package com.example.a24631.testapp.Jump;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.a24631.testapp.R;
import com.example.a24631.testapp.util.ToastUtil;

public class AActivity extends AppCompatActivity {
    private Button JumpToB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        JumpToB=findViewById(R.id.jumpToB);
        JumpToB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AActivity.this,BActivity.class);
                intent.putExtra("extra","extra");
                startActivityForResult(intent,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ToastUtil.showMsg(this, ""+requestCode+resultCode+data.getStringExtra("BExtra"));
    }
}
