package com.example.a24631.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ToastActivity extends AppCompatActivity {

    private Button normal,center,pic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);
        normal = findViewById(R.id.toast_normal);
        center = findViewById(R.id.toast_center);
        pic = findViewById(R.id.toast_pic);
        Onclick onclick = new Onclick();
        normal.setOnClickListener(onclick);
        center.setOnClickListener(onclick);
        pic.setOnClickListener(onclick);
    }

    class Onclick implements Button.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.toast_normal:
                    Toast.makeText(ToastActivity.this, "normal",Toast.LENGTH_LONG).show();
                    break;
                case R.id.toast_center:
                    Toast toast = Toast.makeText(getApplicationContext(), "center",Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                    break;
                case R.id.toast_pic:
                    Toast toast1 = new Toast(getApplicationContext());
                    View view = LayoutInflater.from(ToastActivity.this).inflate(R.layout.layout_toast_item,null);
                    ImageView imageView = (ImageView)view.findViewById(R.id.iv);
                    imageView.setImageResource(R.drawable.image1);
                    TextView textView = (TextView) view.findViewById(R.id.tv);
                    textView.setText("带图片");
                    toast1.setView(view);
                    toast1.show();
                    break;

            }
        }
    }
}
