package com.example.a24631.testapp.RecyclerView;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.a24631.testapp.R;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

public class ListRecyclerViewActivity extends AppCompatActivity {
    private XRecyclerView ListRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_recycler_view);
        ListRecyclerView = findViewById(R.id.listRecyclerView);
        ListRecyclerView.setLayoutManager(new LinearLayoutManager(ListRecyclerViewActivity.this));
        ListRecyclerView.addItemDecoration(new MyDecoration());
        ListRecyclerView.setAdapter(new ListAdapter(this, new ListAdapter.OnItemClickListener() {
            @Override
            public void onClick(int i) {
                Toast.makeText(ListRecyclerViewActivity.this, "坐标"+i, Toast.LENGTH_SHORT).show();
            }
        }));
        ListRecyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                ListRecyclerView.refreshComplete();
            }

            @Override
            public void onLoadMore() {
               // ListRecyclerView.setLimitNumberToCallLoadMore(2);
                ListRecyclerView.loadMoreComplete();
            }
        });
        ListRecyclerView.setRefreshProgressStyle(ProgressStyle.BallClipRotateMultiple);
    }
    class MyDecoration extends RecyclerView.ItemDecoration {
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(0, 0, 0, getResources().getDimensionPixelOffset(R.dimen.height));
        }
    }
}
