package com.example.a24631.testapp.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a24631.testapp.R;


public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private OnItemClickListener onItemClickListener;


    public ListAdapter(Context context, OnItemClickListener onItemClickListener){
        this.mContext=context;
        this.onItemClickListener=onItemClickListener;
    }

    public RecyclerView.ViewHolder  onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if(viewType==0){
            return new ListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_listrecyclerview_item,viewGroup,false));
        }
        else{
            return new ListViewHolder2(LayoutInflater.from(mContext).inflate(R.layout.layout_linear_item,viewGroup,false));
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder  ViewHolder, final int i) {
        if(getItemViewType(i)==0){
            ((ListViewHolder)ViewHolder).textView.setText("面朝大海，春暖花开");
        }
        else{
            ((ListViewHolder2)ViewHolder).textView.setText("海纳百川，有容乃大");
        }
        ViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(i);
                //Toast.makeText(mContext, "pos"+i,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public int getItemCount() {
        return 40;
    }

    @Override
    public int getItemViewType(int position) {
        if(position%2==0){
            return 0;
        }
        else{
            return 1;
        }
    }

    class ListViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.tv);
        }
    }
    class ListViewHolder2 extends RecyclerView.ViewHolder{
        private TextView textView;
        public ListViewHolder2(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.tv);
        }
    }
    public interface OnItemClickListener {
        void onClick(int i);
    }
}
