package com.example.a24631.testapp.MyFragment;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    private FragmentManager fm;
    private List<Fragment> fragmentList;

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    public MyFragmentPagerAdapter(FragmentManager fm,List<Fragment> fragmentList){
        super(fm);
        this.fm=fm;
        this.fragmentList=fragmentList;
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, final int position) {
        return super.instantiateItem(container, position);
    }
}
