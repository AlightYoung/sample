package com.example.a24631.testapp.RecyclerView;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.a24631.testapp.R;

public class HorActivity extends AppCompatActivity {

    private RecyclerView horRv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hor);
        horRv=findViewById(R.id.horRv);
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(HorActivity.this);
        linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        horRv.setLayoutManager(linearLayoutManager);
        horRv.setAdapter(new HorAdapter(HorActivity.this, new HorAdapter.horOnclikListener() {
            @Override
            public void onClick(int pos) {
                Toast.makeText(HorActivity.this,"坐标"+pos,Toast.LENGTH_SHORT).show();
            }
        }));
        horRv.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(0,0,getResources().getDimensionPixelOffset(R.dimen.height), 0);
            }
        });
    }
}
