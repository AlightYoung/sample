package com.example.a24631.testapp.ListView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;

import com.example.a24631.testapp.R;
import com.example.a24631.testapp.util.ToastUtil;

public class ExpandableListViewActivity extends AppCompatActivity {

    private ExpandableListView expandableListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expandable_list_view);
        expandableListView=findViewById(R.id.ExListView);
        expandableListView.setAdapter(new MyExpandableListAdapter());
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int count = new MyExpandableListAdapter().getGroupCount();
                for(int i = 0;i < count;i++){
                    if (i!=groupPosition){
                        expandableListView.collapseGroup(i);
                    }
                }
            }
        });
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ToastUtil.showMsg(ExpandableListViewActivity.this, ""+groupPosition+childPosition);
                return true;
            }
        });
    }
}
