package com.example.a24631.testapp.RecyclerView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.a24631.testapp.R;

public class GridActivity extends AppCompatActivity {

    private RecyclerView gridRv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        gridRv=findViewById(R.id.gridRv);
        gridRv.setLayoutManager(new GridLayoutManager(this, 6));
        gridRv.setAdapter(new gridAdapter(GridActivity.this, new gridAdapter.onitemclickListener() {
            @Override
            public void onclick(int pos) {
                Toast.makeText(GridActivity.this, "坐标"+pos,Toast.LENGTH_SHORT).show();
            }
        }));
    }
}
