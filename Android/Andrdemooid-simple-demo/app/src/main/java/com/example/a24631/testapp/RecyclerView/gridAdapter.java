package com.example.a24631.testapp.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a24631.testapp.R;

public class gridAdapter extends RecyclerView.Adapter<gridAdapter.GridViewHolder> {

    private Context mContext;
    private onitemclickListener listener;

    gridAdapter(Context context,onitemclickListener listener){
        this.mContext=context;
        this.listener=listener;
    }
    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new GridViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_gridrv_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewHolder viewHolder, final int i) {
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onclick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 100;
    }
    class GridViewHolder extends RecyclerView.ViewHolder{
        private TextView tv;
        public GridViewHolder(@NonNull View itemView) {
            super(itemView);
            tv=itemView.findViewById(R.id.tv);
        }
    }
    public interface onitemclickListener {
        void onclick(int pos);
    };
}
