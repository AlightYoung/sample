package com.example.a24631.testapp.ListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.a24631.testapp.R;

public class MyListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public MyListAdapter(Context context){
        this.mContext=context;
        mLayoutInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    static class ViewHolder{
        private ImageView iv_1;
        private TextView tv_1,tv_2,tv_3;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder =null;
        if(convertView==null){
            convertView=mLayoutInflater.inflate(R.layout.layout_list_item,null);
            holder=new ViewHolder();
            holder.iv_1=convertView.findViewById(R.id.iv_1);
            holder.tv_1=convertView.findViewById(R.id.tv_1);
            holder.tv_2=convertView.findViewById(R.id.tv_2);
            holder.tv_3=convertView.findViewById(R.id.tv_3);
            convertView.setTag(holder);
        }else{
            holder= (ViewHolder) convertView.getTag();
        }
        holder.tv_1.setText("这是标题");
        holder.tv_2.setText("这是时间");
        holder.tv_3.setText("这是内容");
        Glide.with(mContext).load("http://bpic.588ku.com/back_pic/04/84/53/1058d89a201d704.jpg!r650/fw/800").into(holder.iv_1);
        return convertView;
    }
}
