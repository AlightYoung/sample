package com.example.a24631.testapp.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.a24631.testapp.R;

public class puAdapter extends RecyclerView.Adapter<puAdapter.puViewHolder> {
    private Context mContext;
    private onItemclickListener listener;
    puAdapter(Context context,onItemclickListener listener){
        this.mContext=context;
        this.listener=listener;
    }
    @NonNull
    @Override

    public puAdapter.puViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new puViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_pu_rv_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull puAdapter.puViewHolder viewHolder, final int i) {
        if(i%2==0) {
            viewHolder.imageView.setImageResource(R.drawable.image1);
        }
        else{
            viewHolder.imageView.setImageResource(R.drawable.image2);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onclick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 30;
    }
    class puViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        public puViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.iv);
        }
    }
    public interface onItemclickListener{
        void onclick(int pos);
    }
}
