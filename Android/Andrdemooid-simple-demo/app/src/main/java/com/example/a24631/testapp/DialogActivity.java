package com.example.a24631.testapp;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.example.a24631.testapp.ListView.MyExpandableListAdapter;
import com.example.a24631.testapp.util.ToastUtil;


public class DialogActivity extends AppCompatActivity {

    private Button dailog1, dailog2, dailog3, dialog4, dialog5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dailog);
        dailog1 = findViewById(R.id.dialog1);
        dailog2 = findViewById(R.id.dialog2);
        dailog3 = findViewById(R.id.dialog3);
        dialog4 = findViewById(R.id.dialog4);
        dialog5 = findViewById(R.id.dialog5);

        MyOnclick myOnclick = new MyOnclick();
        dailog1.setOnClickListener(myOnclick);
        dailog2.setOnClickListener(myOnclick);
        dailog3.setOnClickListener(myOnclick);
        dialog4.setOnClickListener(myOnclick);
        dialog5.setOnClickListener(myOnclick);
    }

    class MyOnclick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = null;
            switch (v.getId()) {
                case R.id.dialog1:
                    builder = new AlertDialog.Builder(DialogActivity.this);
                    builder.setTitle("TessApp").setMessage("你确定吗").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNeutralButton("再想想", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    break;
                case R.id.dialog2:
                    builder = new AlertDialog.Builder(DialogActivity.this);
                    final String[] sex = new String[]{"man", "women"};
                    builder.setTitle("您的性别").setSingleChoiceItems(sex, 0, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(DialogActivity.this, "" + sex[which], Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    }).setCancelable(false);
                    break;
                case R.id.dialog3:
                    builder = new AlertDialog.Builder(DialogActivity.this);
                    final String[] habit = new String[]{"游戏", "阅读", "音乐"};
                    final boolean[] check = new boolean[]{true, true, true};
                    builder.setTitle("您的爱好").setMultiChoiceItems(habit, check, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            Toast.makeText(DialogActivity.this, "" + habit[which] + check[which], Toast.LENGTH_SHORT);

                        }
                    });
                    break;
                case R.id.dialog4:
                    builder = new AlertDialog.Builder(DialogActivity.this);
                    builder.setItems(new String[]{"法师", "射手", "刺客"}, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String message = "";
                            switch (which) {
                                case 0:
                                    message = "法师";
                                    break;
                                case 1:
                                    message = "射手";
                                    break;
                                case 2:
                                    message = "刺客";
                                    break;
                            }
                            ToastUtil.showMsg(DialogActivity.this, message);
                            dialog.dismiss();
                        }
                    });
                    break;
                case R.id.dialog5:
                    builder=new AlertDialog.Builder(DialogActivity.this);
                    View view= LayoutInflater.from(DialogActivity.this).inflate(R.layout.activity_expandable_list_view,null);
                    ExpandableListView expandableListView = view.findViewById(R.id.ExListView);
                    expandableListView.setAdapter(new MyExpandableListAdapter());
                    builder.setView(view);
                    break;
            }
            builder.show();
        }
    }
}
