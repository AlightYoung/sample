package com.example.a24631.testapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.a24631.testapp.util.ToastUtil;

public class ProgressActivity extends AppCompatActivity {

    private ProgressBar pb3;
    private Button btnStart,btnPd1,btnPd2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        pb3=findViewById(R.id.pb_3);
        btnStart=findViewById(R.id.btn_start);
        btnPd1=findViewById(R.id.pd_1);
        btnPd2=findViewById(R.id.pd_2);
        pb3.setProgress(20);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.sendEmptyMessage(0);
            }
        });
        btnPd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog1 = new ProgressDialog(ProgressActivity.this);
                progressDialog1.setTitle("提示");
                progressDialog1.setMessage("正在加载");
                progressDialog1.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Toast.makeText(ProgressActivity.this, "退出",Toast.LENGTH_SHORT).show();
                    }
                });
                //progressDialog1.setCancelable(false);
                progressDialog1.show();
            }
        });
        btnPd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog2 = new ProgressDialog(ProgressActivity.this);
                progressDialog2.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog2.setTitle("提示");
                progressDialog2.setMessage("正在下载...");
               // progressDialog2.setProgress(30);
                progressDialog2.setButton(-1, "cool", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });
                progressDialog2.show();
            }
        });
    }
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(pb3.getProgress()<100){
                handler.postDelayed(runnable,100);
            }else{
                ToastUtil.showMsg(getApplicationContext(), "加载完成");
            }
        }
    };
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            pb3.setProgress(pb3.getProgress()+1);
            handler.sendEmptyMessage(0);
        }
    };
}
