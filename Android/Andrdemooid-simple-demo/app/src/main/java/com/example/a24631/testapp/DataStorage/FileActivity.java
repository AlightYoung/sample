package com.example.a24631.testapp.DataStorage;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.a24631.testapp.R;
import com.example.a24631.testapp.util.ToastUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileActivity extends AppCompatActivity {

    private EditText et;
    private Button save,show;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_file);

        et=findViewById(R.id.et);
        save=findViewById(R.id.save);
        show=findViewById(R.id.show);
        tv=findViewById(R.id.tv);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(et.getText().toString());
                ToastUtil.showMsg(FileActivity.this, "保存成功");
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setText(read());
            }
        });

    }
    public void save(String content){
        FileOutputStream fileOutputStream=null;
        try {
            //内部存储
//            fileOutputStream=openFileOutput("text.txt",MODE_PRIVATE);
            //外部存储
            File dir = new File(Environment.getExternalStorageDirectory(),"yjm");
            if(!dir.exists()){
                dir.mkdirs();
            }
            File file= new File(dir,"text.txt");
            if(!file.exists()){
                file.createNewFile();
            }
            fileOutputStream=new FileOutputStream(file);
            fileOutputStream.write(content.getBytes());
            fileOutputStream.close();
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileOutputStream!=null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public String read(){
        FileInputStream fileInputStream=null;
        try {
            //内部存储
//            fileInputStream=openFileInput("text.txt");
            //外部存储
            File file = new File(Environment.getExternalStorageDirectory()+File.separator+"yjm","text.txt");
            fileInputStream=new FileInputStream(file);
            byte []buff=new byte[1024];
            StringBuilder stringBuilder= new StringBuilder();
            int len;
            while((len=fileInputStream.read(buff))>0){
                stringBuilder.append(new String(buff,0,len));
                return stringBuilder.toString();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fileInputStream!=null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
