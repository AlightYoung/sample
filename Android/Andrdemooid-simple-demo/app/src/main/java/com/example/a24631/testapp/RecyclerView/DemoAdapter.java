package com.example.a24631.testapp.RecyclerView;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a24631.testapp.R;
import com.example.a24631.testapp.util.ToastUtil;

import java.util.List;
import java.util.Map;

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.MyViewHolder> {
    private Context context;
    private List<Map<String,Object>> list;
    public DemoAdapter(Context context,List<Map<String,Object>> list) {
        super();
        this.context=context;
        this.list=list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.layout_demo_item,viewGroup,false);
        MyViewHolder myViewHolder= new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        viewHolder.imageView.setImageResource((Integer) list.get(i).get("img"));
        viewHolder.textView1.setText(list.get(i).get("title").toString());
        viewHolder.textView2.setText(list.get(i).get("info").toString());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.showMsg(context, ""+i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView textView1,textView2;
        private ImageView imageView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView1=itemView.findViewById(R.id.demoTitle);
            textView2=itemView.findViewById(R.id.demoInfo);
            imageView=itemView.findViewById(R.id.demoIv);
        }
    }
}
