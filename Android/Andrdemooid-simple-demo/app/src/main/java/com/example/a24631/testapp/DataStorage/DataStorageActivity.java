package com.example.a24631.testapp.DataStorage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.a24631.testapp.R;

public class DataStorageActivity extends AppCompatActivity {

    private Button SharedPreferences,File;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_storage);
        SharedPreferences=findViewById(R.id.SharedPreferences);
        File=findViewById(R.id.File);


        SharedPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DataStorageActivity.this, SharedPreferencesActivity.class));
            }
        });
        File.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DataStorageActivity.this, FileActivity.class));
            }
        });
    }
}
