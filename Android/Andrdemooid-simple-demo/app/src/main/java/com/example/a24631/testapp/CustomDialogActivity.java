package com.example.a24631.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

import com.example.a24631.testapp.Widgets.CustomDialog;
import com.example.a24631.testapp.util.ToastUtil;

//import com.example.a24631.testapp.custom.customDialog;
public class CustomDialogActivity extends AppCompatActivity {

    private Button textBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_custom_dialog);
        textBtn=findViewById(R.id.btn_text);
        textBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog customDialog = new CustomDialog(CustomDialogActivity.this);
                customDialog.getWindow().setGravity(Gravity.CENTER);
                customDialog.setTitle("提示").setMessage("您确定嘛").setCancel("取消", new CustomDialog.IonCancelListener() {
                    @Override
                    public void onCancel(CustomDialog dialog) {
                        ToastUtil.showMsg(CustomDialogActivity.this, "cancel");
                    }
                }).setConfirm("确定", new CustomDialog.IonConfirmListener() {
                    @Override
                    public void onConfirm(CustomDialog dialog) {
                        ToastUtil.showMsg(CustomDialogActivity.this, "confirm");
                    }
                }).show();
            }
        });


    }
}
