package com.example.a24631.testapp.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a24631.testapp.R;

public class HorAdapter extends RecyclerView.Adapter<HorAdapter.horViewHolder> {
    private Context mContext;
    private horOnclikListener horOnclikListener;

    HorAdapter(Context context,horOnclikListener horOnclikListener){
        this.mContext=context;
        this.horOnclikListener=horOnclikListener;
    }
    @NonNull
    @Override
    public horViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new horViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_hor_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull horViewHolder horViewHolder, final int i) {
        horViewHolder.tv.setText("我是水平item");
        horViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horOnclikListener.onClick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 30;
    }
     class horViewHolder extends RecyclerView.ViewHolder{
        private TextView tv;
         public horViewHolder(@NonNull View itemView) {
             super(itemView);
             tv=itemView.findViewById(R.id.tv);
         }
     }
     public interface horOnclikListener {
         void onClick(int pos);
     }
}
