package com.example.a24631.testapp;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.a24631.testapp.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class SpinnerActivity extends AppCompatActivity {

    private Spinner mSpinner;

    @TargetApi(16)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        mSpinner = findViewById(R.id.spinner);
        List<String> spinnerList = new ArrayList<String>();
        spinnerList.add("北京");
        spinnerList.add("上海");
        spinnerList.add("广州");
        spinnerList.add("深圳");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,spinnerList);
        adapter.setDropDownViewResource(R.layout.spinner_drop_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setDropDownVerticalOffset(100);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    ToastUtil.showMsg(SpinnerActivity.this, "默认值");
                }else {
                    ToastUtil.showMsg(SpinnerActivity.this, "选中" + position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ToastUtil.showMsg(SpinnerActivity.this, "nothing");
            }
        });

    }
}
