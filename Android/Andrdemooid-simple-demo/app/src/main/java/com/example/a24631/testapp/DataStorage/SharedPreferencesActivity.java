package com.example.a24631.testapp.DataStorage;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.a24631.testapp.R;
import com.example.a24631.testapp.util.ToastUtil;

public class SharedPreferencesActivity extends AppCompatActivity {

    private EditText et;
    private Button save,show;
    private TextView tv;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        et=findViewById(R.id.et);
        save=findViewById(R.id.save);
        show=findViewById(R.id.show);
        tv=findViewById(R.id.tv);
        sharedPreferences=getSharedPreferences("data",MODE_PRIVATE);
        editor=sharedPreferences.edit();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("et",et.getText().toString());
                editor.apply();
                ToastUtil.showMsg(SharedPreferencesActivity.this, "保存成功");
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.showMsg(SharedPreferencesActivity.this, sharedPreferences.getString("et",""));
            }
        });
    }
}
