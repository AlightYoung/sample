package com.example.a24631.testapp.RecyclerView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.a24631.testapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DemoActivity extends AppCompatActivity {

    private RecyclerView demoRv;
    private List<Map<String,Object>> listdata=new ArrayList<>();
    private String[]demoTitle={
            "title1","title2","title3","title4","title5","title6"
    };
    private String[]demoInfo={
            "info1","info2","info3","info4","info5","info6"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        demoRv=findViewById(R.id.demoRv);
        demoRv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        demoRv.setAdapter(new DemoAdapter(this, this.getListdata()));
    }
    public List<Map<String,Object>> getListdata(){
        Map<String,Object> map;
        for(int i=0;i<6;i++){
            map=new HashMap<>();
            map.put("title",demoTitle[i]);
            map.put("info",demoInfo[i]);
            map.put("img",R.drawable.image2);
            listdata.add(map);
        }
        return listdata;
    }
}
