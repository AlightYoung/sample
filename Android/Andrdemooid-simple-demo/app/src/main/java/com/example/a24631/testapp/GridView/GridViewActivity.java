package com.example.a24631.testapp.GridView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import com.example.a24631.testapp.R;

public class GridViewActivity extends AppCompatActivity {
    private GridView gv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);
        gv=findViewById(R.id.gv);
        gv.setAdapter(new MyGridViewAdapter(GridViewActivity.this));
    }
}
