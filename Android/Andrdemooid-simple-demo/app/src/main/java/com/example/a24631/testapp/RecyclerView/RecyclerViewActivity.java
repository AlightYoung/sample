package com.example.a24631.testapp.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.a24631.testapp.R;

public class RecyclerViewActivity extends AppCompatActivity {
    private Button listRecycler, horBtn, gridBtn, puBtn, demoBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        listRecycler = findViewById(R.id.listRecyclerViewBtn);
        horBtn = findViewById(R.id.horBtn);
        gridBtn = findViewById(R.id.gridBtn);
        puBtn = findViewById(R.id.puBtn);
        demoBtn = findViewById(R.id.listDemo);
        listRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecyclerViewActivity.this, ListRecyclerViewActivity.class));
            }
        });
        horBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecyclerViewActivity.this, HorActivity.class));
            }
        });
        gridBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecyclerViewActivity.this, GridActivity.class));
            }
        });
        puBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecyclerViewActivity.this, puRecyclerViewActivity.class));
            }
        });
        demoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecyclerViewActivity.this, DemoActivity.class));
            }
        });
    }
}
