package com.example.a24631.testapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;


public class ImageSwitcherActivity extends AppCompatActivity {

    private ImageSwitcher Is;
    private Button changeBtn;
    private int []image = new int[]{R.drawable.image1,R.drawable.image2};
    int index = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_switcher);
        Is=findViewById(R.id.is);
        changeBtn=findViewById(R.id.change);
        Is.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView imageView = new ImageView(ImageSwitcherActivity.this);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setBackgroundColor(0x00000000);
                imageView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
                imageView.setImageResource(image[index]);
                return imageView;
            }
        });
        Is.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        Is.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));
        changeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                if(index>image.length-1){
                    index=index%(image.length);
                }
                Is.setImageResource(image[index]);
            }
        });
    }
}
