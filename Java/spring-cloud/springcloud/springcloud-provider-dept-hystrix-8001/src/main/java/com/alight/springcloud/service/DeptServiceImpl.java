package com.alight.springcloud.service;

import com.alight.springcloud.dao.DeptDao;
import com.alight.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    public boolean addDept(Dept dept) {
        return this.deptDao.addDept(dept);
    }

    public Dept queryById(long id) {
        return this.deptDao.queryById(id);
    }

    public List<Dept> queryAll() {
        return this.deptDao.queryAll();
    }
}
