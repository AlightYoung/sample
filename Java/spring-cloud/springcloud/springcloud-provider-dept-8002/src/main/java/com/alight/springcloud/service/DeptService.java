package com.alight.springcloud.service;

import com.alight.springcloud.pojo.Dept;

import java.util.List;

public interface DeptService {

    public boolean addDept(Dept dept);
    public Dept queryById(long id);
    public List<Dept> queryAll();

}
