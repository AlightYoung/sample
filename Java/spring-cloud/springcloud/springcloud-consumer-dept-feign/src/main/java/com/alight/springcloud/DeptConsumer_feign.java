package com.alight.springcloud;

import com.alight.rules.MyRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.alight.springcloud")
@EnableEurekaClient
//@RibbonClient(name = "SPRINGCLO UD-PROVIDER-DEPT",configuration = MyRule.class)
@EnableFeignClients(basePackages = "com.alight.springcloud")
public class DeptConsumer_feign {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_feign.class,args);
    }
}
