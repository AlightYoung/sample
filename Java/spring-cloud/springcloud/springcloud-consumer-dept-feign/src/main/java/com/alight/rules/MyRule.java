package com.alight.rules;

import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRule {

    // 自定义算法 默认轮询
    @Bean
    public IRule MyRule() {
        return new AlightRandomRule(); //自定义重写randomRule算法
    }
}
