package com.alight.springcloud.controller;

import com.alight.springcloud.pojo.Dept;
import com.alight.springcloud.services.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class DeptConsumerController {

//    @Autowired
//    private RestTemplate restTemplate;

    // ribbon不能写死url，通过服务名来访问
    // private String url = "http://localhost:8001/";
//    private String url = "http://SPRINGCLOUD-PROVIDER-DEPT";

    //feign
    @Autowired
    private DeptClientService deptClientService;

    @RequestMapping("/consumer/dept/add")
    public boolean add( Dept dept){
        System.out.println(dept);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
//        map.add("dname",dept.getDname());
//        return restTemplate.postForObject(url+"/dept/add",map,boolean.class);
        return this.deptClientService.addDept(dept);
    }

    @RequestMapping("/consumer/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id){
//        return restTemplate.getForObject(url+"/dept/get/"+id,Dept.class);
        return this.deptClientService.queryById(id);
    }

    @RequestMapping("/consumer/dept/list")
    public List<Dept> list(){
//        return restTemplate.getForObject(url+"/dept/list",List.class);
        return this.deptClientService.queryAll();
    }
}
