package com.alight.springcloud.controller;

import com.alight.springcloud.pojo.Dept;
import com.alight.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//restful
@RestController
public class DeptController {


    @Autowired
    private DeptService deptService;

    // 获取信息的类 springcloud包下的接口
    @Autowired
    private DiscoveryClient client;

    @PostMapping("/dept/add")
    public boolean addDept(Dept dept){
        System.out.println(dept);
        return deptService.addDept(dept);
    }

    @GetMapping("/dept/get/{id}")
    public Dept queryById(@PathVariable("id") long id){
        return deptService.queryById(id);
    }

    @GetMapping("/dept/list")
    public List<Dept> queryAll(){
        return deptService.queryAll();
    }

    @GetMapping("/dept/discovery")
    // 获取注册进来的微服务的一些信息
    public Object discovery(){
        //获取微服务列表清单
        List<String> services = client.getServices();
        System.out.println(services);
        // 得到一个具体的微服务信息，通过id
        List<ServiceInstance> instances = client.getInstances("SPRINGCLOUD-PROVIDER-DEPT");
        for (ServiceInstance instance : instances) {
            System.out.println(instance.getHost()+instance.getPort()+instance.getUri()+instance.getServiceId());
        }
        return this.client;
    }

}
