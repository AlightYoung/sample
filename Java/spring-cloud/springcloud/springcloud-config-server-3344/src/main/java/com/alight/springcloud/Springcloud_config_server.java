package com.alight.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class Springcloud_config_server {
    public static void main(String[] args) {
        SpringApplication.run(Springcloud_config_server.class,args);
    }
}
