package com.alight.rules;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRule {

    // 自定义算法 默认轮询
    @Bean
    public IRule MyRule() {
        return new RandomRule(); //自定义重写randomRule算法
    }
}
