package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Controller
@RequestMapping("oauth")
public class OAuthController {

    @Value("${client-id}")
    String clientID;
    @Value("${client-secret}")
    String clientSecret;

    @GetMapping("github")
    public String get(String code) {

        RestTemplate restTemplate = new RestTemplate();
        String uri = "https://github.com/login/oauth/access_token";

        //创建请求头
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("client_id", clientID);
        map.add("client_secret", clientSecret);
        map.add("code", code);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(uri, request, String.class);

        String accessTokenStr = response.getBody().toString().trim();

        Map<String, String> resmap = JSON.parseObject(accessTokenStr, Map.class);
        String accessToken = resmap.get("access_token");

        return "getUserInfo?accessToken=" + accessToken;
    }

    @GetMapping("getUserInfo")
    @ResponseBody
    public String getUserInfo(String accessToken) throws IOException {

//      过气api String url = "https://api.github.com/user?access_token="+accessToken;
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.github.com/user";

        HttpHeaders headers = new HttpHeaders();
        // 下面两个不加不行 我也不知道
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        headers.set(HttpHeaders.AUTHORIZATION, "token " + accessToken);

        HttpEntity entity = new HttpEntity(headers);

//        别问为啥下面的getForEntity为啥不行 我还是不知道
//        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url,String.class,entity);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        Map<String, String> map = JSON.parseObject(responseEntity.getBody(), Map.class);
        return map.get("login"); //用户名 其他要拿什么自己取


//        备选httpclient写法
//        CloseableHttpClient aDefault = HttpClients.createDefault();
//        HttpGet request = new HttpGet("https://api.github.com/user");
//        request.addHeader(HttpHeaders.AUTHORIZATION,"token "+accessToken);
//        CloseableHttpResponse response=null;
//        try {
//             response = aDefault.execute(request);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        String info = EntityUtils.toString(response.getEntity());
//        return info;
    }
}
