package com.example.demo.mapper;

import com.example.demo.entity.TUserRoles;
import com.example.demo.entity.TUserRolesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TUserRolesMapper {
    long countByExample(TUserRolesExample example);

    int deleteByExample(TUserRolesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TUserRoles record);

    int insertSelective(TUserRoles record);

    List<TUserRoles> selectByExample(TUserRolesExample example);

    TUserRoles selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TUserRoles record, @Param("example") TUserRolesExample example);

    int updateByExample(@Param("record") TUserRoles record, @Param("example") TUserRolesExample example);

    int updateByPrimaryKeySelective(TUserRoles record);

    int updateByPrimaryKey(TUserRoles record);
}