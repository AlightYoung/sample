package com.alight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringbootJspApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJspApplication.class, args);
    }
//    不要在此处直接运行 以war方式运行
//    在pom.xml配置packing war 同时添加jsp支持 同时在application。properties中进行prefix和suffix的配置
//    然后通过maven的plugins中的spring-boot：run运行即可

}
