package com.alight.rabbitmqexchangedirectpublish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqExchangeDirectPublishApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqExchangeDirectPublishApplication.class, args);
    }

}
