package com.alight.rabbitmqexchangetopicconsumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(
        bindings = @QueueBinding(
                value = @Queue(value = "${mq.config.queue.warn}",autoDelete = "true"),
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.TOPIC),
                key = "*.log.warn"

        )
)
public class WarnReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("receiver:---------warn-----------"+msg);
    }
}
