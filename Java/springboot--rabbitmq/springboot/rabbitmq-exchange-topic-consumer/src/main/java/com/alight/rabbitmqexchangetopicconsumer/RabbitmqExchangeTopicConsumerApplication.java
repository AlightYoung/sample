package com.alight.rabbitmqexchangetopicconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqExchangeTopicConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqExchangeTopicConsumerApplication.class, args);
    }

}
