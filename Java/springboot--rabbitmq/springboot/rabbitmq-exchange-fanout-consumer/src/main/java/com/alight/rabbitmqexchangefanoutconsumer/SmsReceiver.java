package com.alight.rabbitmqexchangefanoutconsumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(
        bindings = @QueueBinding(
                value = @Queue(value = "${mq.config.queue.sms}",autoDelete = "true"),//autoDelete:所有客户端断开连接，删除队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.FANOUT ,autoDelete = "true")//基本同上

        )
)
public class SmsReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("sms-----------receiver:"+msg);
    }

}
