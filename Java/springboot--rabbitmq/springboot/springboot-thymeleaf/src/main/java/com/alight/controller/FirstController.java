package com.alight.controller;

import com.alight.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class FirstController {

    @RequestMapping("/showUser")
    public String showUser(Model model){

        List<User> list = new ArrayList();

        list.add(new User(1,"张三",20));
        list.add(new User(2,"李四",22));
        list.add(new User(3,"王五",25));

        model.addAttribute("list",list);
//        model.addAttribute("msg","");
//        model.addAttribute("data",new Date());
//        model.addAttribute("sex","女");

        return "userList";
    }
}
