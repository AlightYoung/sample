package com.alight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class SpringbootStaticApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootStaticApplication.class, args);
    }

}
