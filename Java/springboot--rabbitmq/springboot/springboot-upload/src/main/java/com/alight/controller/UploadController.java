package com.alight.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// @RestController会转换为json
// 相当于@controller @responseBody
@RestController
public class UploadController {

    @RequestMapping("/upload")
    public Map<String,Object> upload(MultipartFile filename) throws IOException {
        System.out.println(filename.getOriginalFilename());
        filename.transferTo(new File("C:\\Users\\24631\\desktop"+File.separator+filename.getOriginalFilename()));
        Map<String,Object> map = new HashMap();
        map.put("msg","ok");
        System.out.println("");
        return map;
    }
}
