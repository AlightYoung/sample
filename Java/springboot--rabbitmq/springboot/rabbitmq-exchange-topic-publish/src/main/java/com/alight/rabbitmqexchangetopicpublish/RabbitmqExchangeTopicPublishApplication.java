package com.alight.rabbitmqexchangetopicpublish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqExchangeTopicPublishApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqExchangeTopicPublishApplication.class, args);
    }

}
