package com.alight.rabbitmqexchangetopicpublish;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitmqExchangeTopicPublishApplicationTests {

    @Autowired
    private UserSender userSender;

    @Autowired
    private ProductSender productSender;

    @Autowired
    private OrderSender orderSender;

    @Test
    void contextLoads() {
        this.userSender.send("user");
        this.productSender.send("product");
        this.orderSender.send("order");
    }

}
