package com.alight.controller;


import com.alight.pojo.User;
import com.alight.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    //页面跳转
    @RequestMapping("/{page}")
    public String showPage(@PathVariable String page){
        return page;
    }

    @RequestMapping("/addUser")
    public String addUser(User user){
         this.userService.addUser(user);
         return "redirect:/user/selAll";
    }

    @RequestMapping("/selAll")
    public String selAll(Model model){
        List<User> list = this.userService.selAll();
        model.addAttribute("list",list);
        return "userList";
    }

    @RequestMapping("/selById")
    public String selById(Integer id,Model model){
        User user = this.userService.selById(id);
        model.addAttribute("user",user);
        return "user";
    }

    @RequestMapping("/updateUser")
    public String updateUser(User user){
        this.userService.updateUser(user);
        return "redirect:/user/selAll";
    }

    @RequestMapping("delById")
    public String delById(Integer id){
        this.userService.delById(id);
        return "redirect:/user/selAll";
    }
}
