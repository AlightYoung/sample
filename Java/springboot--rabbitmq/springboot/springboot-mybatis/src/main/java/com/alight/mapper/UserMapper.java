package com.alight.mapper;

import com.alight.pojo.User;

import java.util.List;


public interface UserMapper {
     void insertUser(User user);
     List<User> selAll();
     User selById(Integer id);
     void updateUser(User user);
     void delById(Integer id);
}
