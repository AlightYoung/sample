package com.alight.service.impl;


import com.alight.mapper.UserMapper;
import com.alight.pojo.User;
import com.alight.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional//受到事务控制
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public void addUser(User user) {
        this.userMapper.insertUser(user);
    }

    @Override
    public List<User> selAll() {
        return this.userMapper.selAll();
    }

    @Override
    public User selById(Integer id) {
        return this.userMapper.selById(id);
    }

    @Override
    public void updateUser(User user) {
        this.userMapper.updateUser(user);
    }

    @Override
    public void delById(Integer id) {
        this.userMapper.delById(id);
    }
}
