package com.alight.service;

import com.alight.pojo.User;

import java.util.List;


public interface UserService {
    void addUser(User user);
    List<User> selAll();
    User selById(Integer id);
    void updateUser(User user);
    void delById(Integer id);
}
