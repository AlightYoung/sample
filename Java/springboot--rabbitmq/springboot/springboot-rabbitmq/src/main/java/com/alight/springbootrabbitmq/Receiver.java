package com.alight.springbootrabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    @RabbitListener(queues = "FirstQueue")
    public void process(String msg){
        System.out.println("receiver from "+msg);
    }
}
