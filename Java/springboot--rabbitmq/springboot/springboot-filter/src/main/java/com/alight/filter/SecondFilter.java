package com.alight.filter;

import javax.servlet.*;
import java.io.IOException;

/**
* 普通filter通过方法配置
**/
public class SecondFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("进入second filter");
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("离开second filter");
    }

    @Override
    public void destroy() {

    }
}