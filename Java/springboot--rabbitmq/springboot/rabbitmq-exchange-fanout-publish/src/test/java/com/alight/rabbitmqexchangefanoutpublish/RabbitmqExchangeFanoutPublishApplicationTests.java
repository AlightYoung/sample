package com.alight.rabbitmqexchangefanoutpublish;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitmqExchangeFanoutPublishApplicationTests {

    @Autowired
    private Sender sender;

    @Test
    void contextLoads() {
        this.sender.send("广播消息");
    }

}
