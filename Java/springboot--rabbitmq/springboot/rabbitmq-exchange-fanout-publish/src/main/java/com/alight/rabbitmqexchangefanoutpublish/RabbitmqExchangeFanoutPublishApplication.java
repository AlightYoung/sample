package com.alight.rabbitmqexchangefanoutpublish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqExchangeFanoutPublishApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqExchangeFanoutPublishApplication.class, args);
    }

}
