package com.alight.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

// 方法 配置listener

public class SecondListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("listener ...init...");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
