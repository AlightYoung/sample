package com.example.demo.service.api;

import com.example.demo.entity.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
}
