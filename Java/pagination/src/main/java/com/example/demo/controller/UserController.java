package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.example.demo.entity.User;
import com.example.demo.service.MyPageHelper;
import com.example.demo.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MyPageHelper myPageHelper;

    @GetMapping("/getAll")
    @ResponseBody
    public String getAll(){
        List<User> users = userService.getUsers();
        return JSON.toJSON(users).toString();
    }
    @GetMapping("/get/{pageNum}")
    @ResponseBody
    public String getPageByNum(@PathVariable Integer pageNum){
        List<User> users = myPageHelper.getPageInfo(pageNum,3).getList();
        return JSON.toJSON(users).toString();
    }

    @GetMapping("/page/{s}")
    public String toPage(@PathVariable String s){
        return s;
    }
}

