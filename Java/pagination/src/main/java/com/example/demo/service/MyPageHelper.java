package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyPageHelper {

    @Autowired
    private UserMapper userMapper;

    public PageInfo<User> getPageInfo(Integer pageNum, Integer pageSize) {

        //使用PageHelper设置分页，为了安全分页，后边最好紧跟mybatis mapper方法
        //注意这里看起来似乎是属于内存分页，但其实PageHelper插件对mybatis执行流程进行了增强，属于物理分页
        PageHelper.startPage(pageNum,pageSize);
        List<User> users = userMapper.getUsers();

        //返回的是一个PageInfo,包含了分页的所有信息
        PageInfo<User> pageInfo = new PageInfo<>(users);

        return pageInfo;
    }
}
