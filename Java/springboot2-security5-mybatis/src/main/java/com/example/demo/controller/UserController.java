package com.example.demo.controller;

import com.example.demo.entity.UserEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/user")
    public UserEntity getUser() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName("user");
        userEntity.setRoles("USER");
        return userEntity;
    }

    @GetMapping("/admin")
    public UserEntity getAmdin() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName("admin");
        userEntity.setRoles("ADMIN");
        return userEntity;
    }
}
