package com.example.demo.service.api;

import com.example.demo.entity.UserEntity;

public interface UserService {
    UserEntity getUserByName(String name);
}
