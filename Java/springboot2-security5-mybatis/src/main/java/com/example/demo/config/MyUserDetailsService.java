package com.example.demo.config;

import com.example.demo.entity.UserEntity;
import com.example.demo.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        UserEntity userEntity = userService.getUserByName(name);
        if (userEntity == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        //spring security 5 必须加密密码
        return new User(userEntity.getName(),new BCryptPasswordEncoder().encode(userEntity.getPassword()),createAuthority(userEntity.getRoles()));
}

    private List<SimpleGrantedAuthority> createAuthority(String roles){
        String [] roleArray = roles.split(",");
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        for (String role : roleArray){
            authorityList.add(new SimpleGrantedAuthority(role));
        }
        return authorityList;
    }
}
