package com.example.demo.controller;

import com.example.demo.model.Employee;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "用户管理相关接口")
public class EmployeeController {

    @GetMapping("/")
    @ApiOperation("首页接口")
    public String index(){
        return "index";
    }

    @PutMapping("/repetition")
    @ApiOperation("复读接口")
    @ApiImplicitParam(name="str",value = "需要复读的内容",defaultValue = "复读就完事了")
    public String repetition(String str){
        return str;
    }

    @DeleteMapping("add")
    @ApiOperation("小学加法接口")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name="a",value = "第一个加数",defaultValue = "1"),
                    @ApiImplicitParam(name="b",value = "第二个加数",defaultValue = "2")
            }
    )
    public String addTwoNum(int a,int b){
        return ""+(a+b);
    }
    @PostMapping("printEmp")
    @ApiOperation("显示雇员信息接口")
    public String PrintEmp(@RequestBody Employee employee){
        return employee.toString();
    }
}
