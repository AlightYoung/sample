# springboot with swagger
---
### 介绍
一个简单的springboot整合swagger的demo
### 步骤
- pom导入依赖
- 编写配置文件
- 使用swagger
- http://localhost:8080/swagger-ui.html
